import java.util.Scanner;

public class Game {
	private Board board;
	private Player o,x;
	
	public Game() {
		o = new Player('O');
		x = new Player('X');
	}
	public void Play() {
		while (true) {
			Playone();
		}
	}
	
	public void Playone() {
		board = new Board(x,o);
		ShowWelcome();
		while (true) {
			ShowTable();
			ShowTurn();
			Input();
			if (board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		ShowTable();
		ShowWinner();
		ShowStat();
		
	}

	
	

	

	private void ShowStat() {
		System.out.println(x.getName() + " W:D:L -> " + x.getWin() + ":"+ x.getDraw() + ":"+ x.getLose());
		System.out.println(o.getName() + " W:D:L -> " + o.getWin() + ":"+ o.getDraw() + ":"+ o.getLose());

	}

	private void ShowWinner() {
		Player player  = board.getWinner();
		System.out.println(player.getName()+ " Win!!!");
	}

	private void ShowWelcome() {
		System.out.println("Welcome to OX Game!!!");
	}
	private void ShowTable() {
		char [][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i+1);
			for (int j = 0; j < table.length; j++) {
				System.out.print(" "+table[i][j]);
			}
			System.out.println();
		}
	}
	private void ShowTurn() {
		System.out.println(board.getCurrent().getName()+ " Turn");
	}
	private void Input() {
		Scanner kb = new Scanner(System.in);
		while (true) {
			try {
				System.out.print("Please input Row and Col: ");
				String input = kb.nextLine();
				String str [] = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please inpunt Rol and Col : [1-3]");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if (board.setTable(row,col) == false) {
					System.out.println("Table is not free!!!");
					continue;
				}
				break;
			} catch (Exception e) {
				continue;
			}
			
		}
		

	}

}
